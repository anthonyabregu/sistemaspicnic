<?php 
    /*$tipoUsuario = $this->session->userdata('tipoUsuario');*/
    $idUsuario = $this->session->userdata('idUsuario');
    $nombreUsuario = $this->session->userdata('nombreUsuario');
 ?>
<style>
    .table th, .table td{
        font-size:12px;
        padding: 5px;
    }
</style>

<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/actividad.css">

<div class="modal" tabindex="-1" role="dialog" id="modaleliminar">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Eliminar Cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>¿Desea eliminar al cliente #<span class="id"></span>?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-danger btn-eliminar">Eliminar</button>
      </div>
    </div>
  </div>
</div>

<div class="container" id="cliente">
    <h1 class="page-title">Clientes</h1>
    <br>
    <button type="button" class="btn btn-success btn-agregar btn-trans" data-toggle="modal" data-target="#modalagregar" ><i class="fas fa-plus-circle"></i> Agregar</button>
    <br>
    <br>
    <form action="<?php echo base_url(); ?>actividad/reportecsv" method="post" hidden>
        <div class="row filtros">
            <div class="col-sm-3">
                <div class="card-box">
                    <label>Proyecto</label>
                    <select class="form-control" name="proyecto">
                        <option value="">Seleccione...</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card-box">
                    <label>Fecha</label>
                    <input type="month" name="fecha" class="form-control">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="card-box">
                    <button type="submit" class=" btn btn-success btn-reporte">Reporte CSV</button>
                </div>
            </div>
            <div class="col-sm-4 right">
                <div class="card-box">
                    <label id="horas"></label>
                </div>
            </div>
        </div>
    </form>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th width="30">ID</th>
                            <th width="90">Nombre</th>
                            <th width="50">Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="lista">

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="modalagregar">
        <div class="modal-dialog" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">Agregar Cliente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo base_url(); ?>cliente/agregar" method="post" autocomplete="off">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">NOMBRE</label>
                            <input type="text" name="nombre" value="" class="form-control" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Agregar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal" tabindex="-1" role="dialog" id="modaleditar">
        <div class="modal-dialog" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title">Ver Tarea</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?php echo base_url(); ?>cliente/actualizar" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">ID</label>
                            <input type="text" name="id" value="" class="form-control" readonly>
                        </div>
                        <div class="form-group">
                            <label for="">Nombre</label>
                            <input type="text" name="nombre" class="form-control" required>
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <!--<a class="eliminar" href="#modaleliminar" data-toggle="modal" style="position:absolute; left:15px; color:red">Eliminar</a> -->
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <div class="modal" tabindex="-1" role="dialog" id="modaleliminar">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">¡Alerta!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>¿Está seguro que desea eliminar este Encargado?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-eliminar">Eliminar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>


</div>
