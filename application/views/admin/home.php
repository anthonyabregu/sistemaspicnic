<?php $tipo = $this->session->userdata('tipo'); ?>

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/home.css?v=1.4">

<div class="container">
  <h1>Dashboard</h1>
  <br>
  <div class="row">

      <?php if($tipo == 1) {?>
   
        <div class="col-sm-3">
            <a class="item" href="<?php echo base_url(); ?>admin/actividad">
                <div class="icono">
                    <i class="fa fa-address-book icono"></i>
                </div>
                <div class="nom">Actividad</div>
            </a>
        </div>
        <div class="col-sm-3">
          <a class="item" href="<?php echo base_url(); ?>admin/cliente">
              <div class="icono"><i class="fa fa-user icono"></i></div>
              <div class="nom">Clientes</div>
          </a>
        </div>
        <div class="col-sm-3">
          <a class="item" href="<?php echo base_url(); ?>admin/proyecto">
              <div class="icono"><i class="fas fa-project-diagram icono"></i></div>
              <div class="nom">Proyectos</div>
          </a>
        </div>
        <div class="col-sm-3">
          <a class="item" href="<?php echo base_url(); ?>admin/presupuesto">
              <div class="icono"><i class="fas fa-dollar-sign icono"></i></div>
              <div class="nom">Presupuesto</div>
          </a>
        </div>
      <?php } else { ?>

        <div class="col-sm-3">
            <a class="item" href="<?php echo base_url(); ?>admin/actividad">
                <div class="icono">
                    <i class="fa fa-address-book icono"></i>
                </div>
                <div class="nom">Actividad</div>
            </a>
        </div>

      <?php } ?>  
      <!--<div class="col-sm-3">
          <a class="item" href="<?php echo base_url(); ?>admin/proyectos">
              <div class="icono"><i class="fas fa-project-diagram icono"></i></div>
              <div class="nom">Proyectos</div>
          </a>
      </div>
      <div class="col-sm-3">
          <a class="item" href="<?php echo base_url(); ?>admin/tipo_proyecto">
              <div class="icono"><i class="fas fa-layer-group icono"></i></div>
              <div class="nom">Tipo Proyecto</div>
          </a>
      </div>
      <div class="col-sm-3">
          <a class="item" href="<?php echo base_url(); ?>admin/Encargado">
              <div class="icono">
                  <i class="fa fa-user icono"></i>
              </div>
              <div class="nom">Encargados</div>
          </a>
      </div>

      <div class="col-sm-3">
            <a class="item" href="<?php echo base_url(); ?>admin/usuariowsp">
                <div class="icono">
                    <i class="fab fa-whatsapp icono"></i>
                </div>
                <div class="nom">Consultas Whatsapp</div>
            </a>
      </div>

    
  

    
      <div class="col-sm-3">
          <a class="item" href="<?php echo base_url(); ?>admin/contacto">
              <div class="icono">
                  <i class="fa fa-address-book icono"></i>
              </div>
              <div class="nom">Contactos</div>
          </a>
      </div>


    

    
      <div class="col-sm-3">
          <a class="item" href="<?php echo base_url(); ?>admin/origen_referencia">
              <div class="icono">
                  <i class="fas fa-user-friends icono"></i>
              </div>
              <div class="nom">Referidos</div>
          </a>
      </div>
    

    
      <div class="col-sm-3">
          <a class="item" href="<?php echo base_url(); ?>admin/usuariowsp">
            <div class="icono">
              <i class="fab fa-whatsapp icono"></i>
            </div>
            <div class="nom">Consultas Whatsapp</div>
          </a>
      </div>

    -->
    
  </div>
</div>
