<?php

	class Presupuesto_model extends CI_Model{

		function __construct(){

			parent::__construct();
		}

		public function listar($cln_id, $pry_id, $fecha, $where, $order_by){

			$sql = "SELECT prs.*, pry.pry_descripcion, cln.cln_descripcion FROM presupuesto as prs
					inner join proyecto as pry on prs.pry_id = pry.pry_id inner join cliente as cln
					on prs.cln_id = cln.cln_id  ". $where . $order_by;

            if($cln_id == "" && $pry_id == "" && $fecha == ""){
                 $query = $this->db->query($sql);
            }else{
                if($cln_id != "" && $pry_id != "" && $fecha != ""){ $query = $this->db->query($sql, array($cln_id, $pry_id, $fecha)); }
                if($cln_id != "" && $pry_id == "" && $fecha == ""){ $query = $this->db->query($sql, array($cln_id)); }
                if($cln_id != "" && $pry_id != "" && $fecha == ""){ $query = $this->db->query($sql, array($cln_id, $pry_id)); }
                if($cln_id != "" && $pry_id == "" && $fecha != ""){ $query = $this->db->query($sql, array($cln_id, $fecha)); }
                if($cln_id == "" && $pry_id != "" && $fecha == ""){ $query = $this->db->query($sql, array($pry_id)); }
                if($cln_id == "" && $pry_id != "" && $fecha != ""){ $query = $this->db->query($sql, array($pry_id, $fecha)); }
                if($cln_id == "" && $pry_id == "" && $fecha != ""){ $query = $this->db->query($sql, array($fecha)); }
            }

            //$query = $this->db->query($sql);

            if($query->num_rows()>0){
                return $query->result();
            }else{
                return FALSE;
            }
		}

		public function agregar($param){

			return $this->db->insert('presupuesto', $param);
		}

		public function editar($param, $id){

            $this->db->where('prs_id', $id);
            $result =  $this->db->update('presupuesto', $param); 

            return $result;
        }

        public function eliminar($id){
            $sql = "delete from presupuesto where prs_id = ? ";
            $query = $this->db->query($sql,array($id));

        }
	}
?>