$(document).ready(function () {

	listar();

    listarproyecto();
	
    new Request("cliente/listar/",{
        
    },function(res){
        console.log(res);
        $.each(res.lista,function(k,v){
            var option = '<option value="'+v.cln_id+'">'+v.cln_descripcion+'</option>'
            $("select[name=cliente]").append(option);
            // $("#modalagregar #cat_add select[name=categoria]").append(option);
        });

         
    });

    $(".filtros input[name=fecha]").bind("change",function(){
        var fecha = $(this).val();
        //alert(fecha);
        listar();
        totalHoras();

    })

    $(".filtros select[name=proyecto]").bind("change",function(){
        
        listar();
        totalHoras();

    })

    $(".filtros select[name=cliente]").bind("change",function(){
        
        listar();
        listarproyecto();
        totalHoras();

    })


    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {

                $("#modalagregar").modal("hide");
                $("#modalagregar select[name=proyecto]").val("");
                listar();

            }else{
            	alert("No se pudo agregar la actividad");
            }
        }
    }

    $("#modalagregar form").ajaxForm(options);


    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {

                $("#modaleditar").modal("hide");
                listar();
                totalHoras();

            }else{
                alert("No se pudo editar la actividad");
            }
        }
    }

    $("#modaleditar form").ajaxForm(options);

});


function listar() {
    
    var fecha = $(".filtros input[name=fecha]").val();
    var proyecto = $(".filtros select[name=proyecto]").val();
    var cliente = $(".filtros select[name=cliente]").val();

    $("#actividad .lista").empty();

    new Request("actividad/listar/", {
        cliente: cliente,
        proyecto: proyecto,
        fecha: fecha
    }, function (res) {
        console.log(res);
        $("#actividad .lista").empty();
        if(res.res == "ok"){
        	$.each(res.lista, function (k, v) {
	            var it = new ItemActividad(v);
	            $("#actividad .lista").append(it);
        	})
        }else{
        	alert("No hay registros");
        }
        
            

    });


}



var ItemActividad = function (data) {

    var id = data.id;
    var idproyecto = data.idproyecto;
    var nombre = data.nombre;
    var proyecto = data.proyecto;
    var tiempo = data.tiempo;
    var tarea = data.tarea;
    var fecha = data.fecha;
    var cliente = data.cliente;
    var html = $('<tr width="100%">' +
        '<td style="vertical-align:middle;">' + id + '</td>' +
        '<td style="vertical-align:middle;">' + fecha + '</td>' +
        '<td style="vertical-align:middle;">' + nombre + '</td>' +
        '<td style="vertical-align:middle;">' + cliente + '</td>' +
        '<td style="vertical-align:middle;">' + proyecto + '</td>' +
        '<td style="vertical-align:middle;">' + tiempo + ' hora(s)' + '</td>' +
        '<td style="vertical-align:middle;">' + tarea  + '</td>' +
        '<td><div class="btn-group" role="group" aria-label="...">' +
        '<button type="button" class="btn btn-primary btn-sm btn-editar" title="Editar Horas"><i class="fa fa-edit"></i></button>' +
        '<button type="button" class="btn btn-danger btn-sm btn-eliminar" ><i class="fa fa-minus-circle"></i></button>' +
        '</div></td>' +
        '</tr>');

        html.find(".btn-editar").click(function () {

            $("#modaleditar input[name=id]").val(id);
            $("#modaleditar input[name=horas]").val(tiempo);
            $("#modaleditar select[name=proyecto]").val(idproyecto);
            $("#modaleditar textarea[name=tarea]").val(tarea);
            $("#modaleditar input[name=fecha]").val(fecha);
            $("#modaleditar").modal("show");

            /*$("#modaleditar a.eliminar").unbind();
            $("#modaleditar a.eliminar").click(function () {
                $("#modaleditar").modal("hide");
                $("#modaleliminar .btn-eliminar").unbind();
                $("#modaleliminar .btn-eliminar").click(function () {

                    $("#modaleliminar").modal("show");
                    new Request("encargado/eliminar/" + id, {
                    }, function (res) {
                        listar();
                        $("#modaleliminar").modal("hide");
                    });
                });

            })*/


        });

        html.find(".btn-eliminar").click(function () {

            $("#modaleliminar").modal("show");
            $("#modaleliminar .id").html(id);


            $("#modaleliminar .btn-eliminar").unbind();
            $("#modaleliminar .btn-eliminar").click(function () {
                $("#modaleliminar").modal("hide");
                new Request("actividad/eliminar/" + id, {
                }, function (res) {
                    //listar();
                    html.remove();
                });
               
            })


        });

    return html;
};


function listarproyecto(){

    var cliente = $(".filtros select[name=cliente]").val();

    new Request("proyecto/listar/",{
        cliente: cliente
    },function(res){
        $("select[name=proyecto]").html("");
        console.log(res);
        $("select[name=proyecto]").append("<option value=''>Seleccione...</option>");
        $.each(res.lista,function(k,v){
            var option = '<option value="'+v.pry_id+'">'+v.cln_descripcion+' - '+v.pry_descripcion+'</option>'
            $("select[name=proyecto]").append(option);
        });

         
    });

}


function totalHoras(){

    var fecha = $(".filtros input[name=fecha]").val();
    var proyecto = $(".filtros select[name=proyecto]").val();
    var cliente = $(".filtros select[name=cliente]").val();

    new Request("actividad/totalHoras/", {
        cliente: cliente,
        proyecto: proyecto,
        fecha: fecha
    }, function (res) {
        console.log(res);
        var horas;
        if(res.res == "ok"){
            if(res.lista == "indefinido"){
                $("#horas").text('');
            }else{
                $.each(res.lista, function (k, v) {
                    horas = v.totalHoras;
                    if(horas == "" || horas == null){
                        $("#horas").text('No se registraron horas');
                    }else{
                        $("#horas").text('Total: ' + horas + ' hora(s)');
                    }
                })
            }
            
        }else{
            alert("No hay registros");
        }
         

    });
}
