$(document).ready(function () {

    listar();

    //$("#categorias .filtros select[name=estado]").bind("change",listar);

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modalcontacto").modal("hide");
                listar();
            }
        }
    }

    $("#modalcontacto form").ajaxForm(options);

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modaleditar").modal("hide");
                listar();
            }
        }
    }
    $("#modaleditar form").ajaxForm(options);

});


function listar() {console.log('log');
    $("#contacto .lista").empty();

    new Request("contacto/listar/", {

    }, function (res) {
        $("#contacto .lista").empty();
        $.each(res, function (k, v) {
            var it = new ItemContacto(v);
            $("#contacto .lista").append(it);
        })

    });


}


var ItemContacto = function (data) {

    var id = data.cto_id;
    var nombre = data.cto_nombre;
    var email = data.cto_email;
    var telefono = data.cto_telefono;
    var mensaje = data.cto_mensaje;

    var html = $('<tr width="100%">' +
        '<td style="vertical-align:middle;">' + id + '</td>' +
        '<td style="vertical-align:middle;">' + nombre + '</td>' +
        '<td style="vertical-align:middle;">' + email + '</td>' +
        '<td style="vertical-align:middle;">' + telefono + '</td>' +
        '<td><div class="btn-group" role="group" aria-label="...">' +
        '<button type="button" class="btn b tn-outline-primary btn-editar" ><i class="fa fa-eye"></i> Detalle</button>' +
        '</div></td>' +
        '</tr>');


    html.find(".btn-editar").click(function () {


        $("#modaleditar input[name=id]").val(id);
        $("#modaleditar input[name=nombre]").val(nombre);
        $("#modaleditar input[name=email]").val(email);
        $("#modaleditar input[name=telefono]").val(telefono);
        $("#modaleditar textarea[name=mensaje]").val(mensaje);

        $("#modaleditar").modal("show");

        $("#modaleditar a.eliminar").unbind();
        $("#modaleditar a.eliminar").click(function () {
            $("#modaleditar").modal("hide");
            $("#modaleliminar .btn-eliminar").unbind();
            $("#modaleliminar .btn-eliminar").click(function () {

                new Request("contacto/eliminar/" + data.cto_id, {
                }, function (res) {
                    listar();
                    $("#modaleliminar").modal("hide");
                });
            });

        })


    });

    return html;

};
