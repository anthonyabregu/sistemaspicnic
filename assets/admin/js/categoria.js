$(document).ready(function () {

    listar();

    $("#categorias .filtros select[name=estado]").bind("change",listar);

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modalagregar").modal("hide");
                $("#categorias .filtros select[name=estado]").val(res.estado);
                listar();
            }
        }
    }

    $("#modalagregar form").ajaxForm(options);

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modaleditar").modal("hide");
                $("#categorias .filtros select[name=estado]").val(res.estado);
                listar();
            }
        }
    }
    $("#modaleditar form").ajaxForm(options);

});


function listar() {console.log('log');
    $("#categorias .lista").empty();

    var es = $(".filtros select[name=estado]").val();
    console.log(es);
    new Request("categoria/listar/", {
        estado: es
    }, function (res) {
        $("#categorias .lista").empty();
        $.each(res, function (k, v) {
            var it = new ItemCategoria(v);
            $("#categorias .lista").append(it);
        })

    });


}


var ItemCategoria = function (data) {

    var categoria_id = data.ctg_id;
    var categoria_nombre = data.ctg_nombre;

    var html = $('<tr width="100%">' +
        '<td>' + categoria_id + '</td>' +
        '<td>' + categoria_nombre + '</td>' +
        '<td><div class="btn-group" role="group" aria-label="...">' +
        '<button type="button" class="btn b tn-outline-primary btn-editar"><i class="fas fa-edit"></i> Editar</button>' +
        '</div></td>' +
        '</tr>');


    html.find(".btn-editar").click(function () {


        $("#modaleditar input[name=id]").val(categoria_id);
        $("#modaleditar input[name=nombre]").val(categoria_nombre);
        $("#modaleditar select[name=estado]").val(data.ctg_estado);

        $("#modaleditar").modal("show");

        $("#modaleditar a.eliminar").unbind();
        $("#modaleditar a.eliminar").click(function () {
            $("#modaleditar").modal("hide");
            $("#modaleliminar .btn-eliminar").unbind();
            $("#modaleliminar .btn-eliminar").click(function () {

                new Request("categoria/eliminar/" + data.ctg_id, {
                }, function (res) {
                    listar(res.ctg_estado);
                    $("#modaleliminar").modal("hide");
                });
            });

        })


    });

    return html;

};
