$(document).ready(function () {

    $("#proyecto .filtros select[name=estado]").bind("change",listar);

    $("#proyecto .filtros select[name=tipoproyecto]").bind("change",listar);


    ubigeo.cargar(function(){

            $.each(ubigeo.departamentos(),function(k,v){

                var op = '<option value="'+v.ubg_id.substr(0,2)+'">'+v.ubg_departamento+'</option>';
                $("select[name=departamento]").append(op);


                $("#modalagregar select[name=departamento]").bind("change",function(){

                    $("#modalagregar select[name=provincia]").empty();
                    $("#modalagregar select[name=distrito]").empty();
                    $("#modalagregar select[name=provincia]").append('<option value="">Seleccione...</option>');

                    var dep = $(this).val();
                    $.each(ubigeo.provincias(dep),function(k,v){

                        var op = '<option value="'+v.ubg_id.substr(0,4)+'">'+v.ubg_provincia+'</option>';
                        $("#modalagregar select[name=provincia]").append(op);

                    });

                    if($(this).val()=="15" || $(this).val()=="07"){
                        $("#modalagregar select[name=recojo]").attr("disabled",false);
                        $("#modalagregar select[name=recojo]").attr("required",true);
                    }else{
                        $("#modalagregar select[name=recojo]").attr("disabled",true);
                        $("#modalagregar select[name=recojo]").attr("required",false);
                        $("#modalagregar select[name=recojo]").val("");
                    }
                })

                $("#modalagregar select[name=provincia]").bind("change",function(){

                    $("#modalagregar select[name=distrito]").empty();
                    $("#modalagregar select[name=distrito]").append('<option value="">Seleccione...</option>');

                    var prov = $(this).val();
                    $.each(ubigeo.distritos(prov),function(k,v){

                        var op = '<option value="'+v.ubg_id+'">'+v.ubg_distrito+'</option>';
                        $("#modalagregar select[name=distrito]").append(op);

                    });
                });


                $("#modaleditar select[name=departamento]").bind("change",function(){

                    $("#modaleditar select[name=provincia]").empty();
                    $("#modaleditar select[name=distrito]").empty();
                    $("#modaleditar select[name=provincia]").append('<option value="">Seleccione...</option>');

                    var dep = $(this).val();
                    $.each(ubigeo.provincias(dep),function(k,v){

                        var op = '<option value="'+v.ubg_id.substr(0,4)+'">'+v.ubg_provincia+'</option>';
                        $("#modaleditar select[name=provincia]").append(op);

                    });

                    if($(this).val()=="15" || $(this).val()=="07"){
                        $("#modaleditar select[name=recojo]").attr("disabled",false);
                        $("#modaleditar select[name=recojo]").attr("required",true);
                    }else{
                        $("#modaleditar select[name=recojo]").attr("disabled",true);
                        $("#modaleditar select[name=recojo]").attr("required",false);
                        $("#modaleditar select[name=recojo]").val("");
                    }
                })

                $("#modaleditar select[name=provincia]").bind("change",function(){

                    $("#modaleditar select[name=distrito]").empty();
                    $("#modaleditar select[name=distrito]").append('<option value="">Seleccione...</option>');

                    var prov = $(this).val();
                    $.each(ubigeo.distritos(prov),function(k,v){

                        var op = '<option value="'+v.ubg_id+'">'+v.ubg_distrito+'</option>';
                        $("#modaleditar select[name=distrito]").append(op);

                    });
                });


                $(".filtros select[name=departamento]").bind("change",function(){

                    $(".filtros select[name=provincia]").empty();
                    $(".filtros select[name=distrito]").empty();
                    $(".filtros select[name=provincia]").append('<option value="">Todas</option>');
                    $(".filtros select[name=distrito]").append('<option value="">Todos</option>');

                    var dep = $(this).val();
                    $.each(ubigeo.provincias(dep),function(k,v){

                        var op = '<option value="'+v.ubg_id.substr(0,4)+'">'+v.ubg_provincia+'</option>';
                        $(".filtros select[name=provincia]").append(op);

                    });
                })

                $(".filtros select[name=provincia]").bind("change",function(){

                    $(".filtros select[name=distrito]").empty();
                    $(".filtros select[name=distrito]").append('<option value="">Todos</option>');

                    var prov = $(this).val();
                    $.each(ubigeo.distritos(prov),function(k,v){

                        var op = '<option value="'+v.ubg_id+'">'+v.ubg_distrito+'</option>';
                        $(".filtros select[name=distrito]").append(op);

                    });
                });

            });
    });


    new Request("tipo_proyecto/listar/",{
        estado: 1
    },function(res){
        console.log(res);
        $.each(res,function(k,v){
            var option = '<option value="'+v.tip_id+'">'+v.tip_descripcion+'</option>'
            $("select[name=tipoproyecto]").append(option);
            
        });

         listar();
    });


    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modalagregar").modal("hide");
                listar();
            }
        }
    }

    $("#modalagregar form").ajaxForm(options);

    var options = {
        dataType: 'json',
        type: 'post',
        clearForm: true,
        success: function (res) {
            if (res.res == "ok") {
                $("#modaleditar").modal("hide");
                $(".filtros select[name=estado]").val(res.estado);
                listar(res.estado);
                
            }
        }
    }
    $("#modaleditar form").ajaxForm(options);

    /*html.find(".btn-editar").click(function () {
        alert("HOLA");
        $("#modaleditar").modal("show");
    });*/

});



function listar() {

    var tip = $(".filtros select[name=tipoproyecto]").val();
    var es = $(".filtros select[name=estado]").val();

    $("#proyecto .lista").empty();

    new Request("proyecto/listar/", {
        estado: es, 
        tipo: tip
    }, function (res) {
        console.log(res);
        if(res == "empty"){
            alert("No existen proyectos en este tipo y estado.");
        }else{
            $("#proyecto .lista").empty();
            $.each(res, function (k, v) {
                var it = new ItemProyecto(v);
                $("#proyecto .lista").append(it);
            })
        }
        

    });


}


var ItemProyecto = function (data) {

    var id = data.pry_id;
    var descripcion = data.pry_descripcion;
    var tip_id = data.tip_id;
    var estado = data.pry_estado;
    var ubg_id = data.ubg_id;

   

    //var departamento = ubigeo[0].ubg_departamento;

    var html = $('<tr width="100%">' +
        '<td style="vertical-align:middle;">' + id + '</td>' +
        '<td style="vertical-align:middle;">' + descripcion + '</td>' +
        '<td style="vertical-align:middle;"><div class="btn-group" role="group" aria-label="...">' +
        '<button type="button" class="btn btn-outline-primary btn-editar"><i class="fas fa-edit"></i> Editar</button>' +
        '<a href="'+ path +'admin/designar_encargado/'+ id +'" class="btn btn-primary encargado"><i class="fas fa-eye"></i> Encargados</a>' +
        '</div></td>' +
        '</tr>');

        /*    var html = $('<tr width="100%" data-id="' + id + '">' +
                    '<td>' + data.pry_titulo + '</td>' +
                    '<td>' + data.pry_orden + '</td>' +
                    '<td><div class="btn-group" role="group" aria-label="...">' +
                    '<button type="button" class="btn btn-outline-primary btn-editar"><i class="fas fa-edit"></i> Editar</button>' +
                    '</div></td>' +
                '</tr>');*/



        html.find(".btn-editar").click(function () {

            //var departamento = ubigeo_id(ubg_id);
            //console.log(departamento);

            $("#modaleditar input[name=id]").val(id);
            $("#modaleditar input[name=proyecto]").val(descripcion);
            $("#modaleditar select[name=tipoproyecto]").val(tip_id);
            $("#modaleditar select[name=estado]").val(estado);

            $("#modaleditar").modal("show");

            if(data.ubg_id!=null){

                $("#modaleditar select[name=departamento]").val(data.ubg_id.substr(0,2));
                $("#modaleditar select[name=provincia]").empty();
                $("#modaleditar select[name=distrito]").empty();

                var dep = data.ubg_id.substr(0,2);
                $.each(ubigeo.provincias(dep),function(k,v){

                    var op = '<option value="'+v.ubg_id.substr(0,4)+'">'+v.ubg_provincia+'</option>';
                    $("#modaleditar select[name=provincia]").append(op);

                });

                $("#modaleditar select[name=provincia]").val(data.ubg_id.substr(0,4));

                var prov = data.ubg_id.substr(0,4);
                $.each(ubigeo.distritos(prov),function(k,v){

                    var op = '<option value="'+v.ubg_id.substr(0,6)+'">'+v.ubg_distrito+'</option>';
                    $("#modaleditar select[name=distrito]").append(op);

                });
                $("#modaleditar select[name=distrito]").val(data.ubg_id.substr(0,6));
            }



            $("#modaleditar a.eliminar").unbind();
            $("#modaleditar a.eliminar").click(function () {
                $("#modaleditar").modal("hide");
                $("#modaleliminar .btn-eliminar").unbind();
                $("#modaleliminar .btn-eliminar").click(function () {

                    $("#modaleliminar").modal("show");
                    new Request("proyecto/eliminar/" + id, {
                    }, function (res) {
                        listar();
                        $("#modaleliminar").modal("hide");
                    });
                });

            })


        });

    return html;
};

/*function ubigeo_id(ubg_id){

    new Request("ubigeo/ubigeo_id/",{
            ubg_id: ubg_id
    },function(res){
        console.log(res);
        var ubigeo = res[0].ubg_id + "/" + res[0].ubg_departamento + "/" + res[0].ubg_provincia + "/" + res[0].ubg_distrito;
        
        return res[0].ubg_departamento;

         //listar();
    });
}*/



function listar_ubigeo(){

    new Request("ubigeo/listar/",{
            ubg_id: ubg_id
    },function(res){
        
        $("#modaleditar select[name=departamento]").html('');
        $("#modaleditar select[name=provincia]").html('');
        $("#modaleditar select[name=distrito]").html('');
        console.log(res);
        $.each(res,function(k,v){
            var option_dep = '<option value="'+v.ubg_departamento+'">'+v.ubg_departamento+'</option>'
            var option_pro = '<option value="'+v.ubg_provincia+'">'+v.ubg_provincia+'</option>'
            var option_dis = '<option value="'+v.ubg_id+'">'+v.ubg_distrito+'</option>'
            
            $("#modaleditar select[name=departamento]").append(option_dep);
            $("#modaleditar select[name=provincia]").append(option_pro);
            $("#modaleditar select[name=distrito]").append(option_dis);
            // $("#modalagregar #cat_add select[name=categoria]").append(option);
        });

         //listar();
    });
}